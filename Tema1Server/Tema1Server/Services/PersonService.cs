using Grpc.Core;
using GrpcServiceTema1;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Tema1Server
{
    public class PersonService : Person.PersonBase
    {
        private readonly ILogger<PersonService> _logger;
        public PersonService(ILogger<PersonService> logger)
        {
            _logger = logger;
        }

        public override Task<PersonDataReply> Person(PersonDataRequest request, ServerCallContext context)
        {
            PersonData person = new PersonData(request.FullName, request.Cnp);
            Console.WriteLine(person);

            return Task.FromResult(new PersonDataReply
            {
                Age = person.GetAge(),
                Gender = (PersonDataReply.Types.Gender)person.GetGender()
            });
        }
    }
}
