﻿using System;
using System.Text;

namespace GrpcServiceTema1
{
    public enum Gender
    {
        MALE = 0,
        FEMALE = 1
    }
    public class PersonData
    {
        public string FullName
        {
            get; private set;
        }
        public string CNP
        {
            get; private set;
        }
        public PersonData(string fullName, string CNP)
        {
            FullName = fullName;
            this.CNP = CNP;
        }

        public int GetAge()
        {
            int year = System.Globalization.CultureInfo.CurrentCulture.Calendar.ToFourDigitYear(int.Parse(CNP.Substring(1, 2)));
            int month = int.Parse(CNP.Substring(3, 2));
            int day = int.Parse(CNP.Substring(5, 2));
            DateTime dateOfBirth = new DateTime(year, month, day);
            DateTime today = DateTime.Today;
            return Convert.ToInt32(((today - dateOfBirth).TotalDays) / 365);
        }

        public Gender GetGender()
        {
            int genderDigit = int.Parse(CNP.Substring(0, 1));
            if (genderDigit == 1 || genderDigit == 5)
            {
                return Gender.MALE;
            }
            else
            {
                return Gender.FEMALE;
            }
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("Full name: ");
            stringBuilder.Append(FullName);
            stringBuilder.Append("\n");
            stringBuilder.Append("CNP: ");
            stringBuilder.Append(CNP);
            stringBuilder.Append("\n");
            stringBuilder.Append("Gender: ");
            stringBuilder.Append(GetGender());
            stringBuilder.Append("\n");
            stringBuilder.Append("Age: ");
            stringBuilder.Append(GetAge());
            return stringBuilder.ToString();
        }
    }
}
