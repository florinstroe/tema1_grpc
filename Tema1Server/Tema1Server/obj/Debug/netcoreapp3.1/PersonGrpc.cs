// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: Protos/person.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Tema1Server {
  public static partial class Person
  {
    static readonly string __ServiceName = "person.Person";

    static readonly grpc::Marshaller<global::Tema1Server.PersonDataRequest> __Marshaller_person_PersonDataRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Tema1Server.PersonDataRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Tema1Server.PersonDataReply> __Marshaller_person_PersonDataReply = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Tema1Server.PersonDataReply.Parser.ParseFrom);

    static readonly grpc::Method<global::Tema1Server.PersonDataRequest, global::Tema1Server.PersonDataReply> __Method_Person = new grpc::Method<global::Tema1Server.PersonDataRequest, global::Tema1Server.PersonDataReply>(
        grpc::MethodType.Unary,
        __ServiceName,
        "Person",
        __Marshaller_person_PersonDataRequest,
        __Marshaller_person_PersonDataReply);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Tema1Server.PersonReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of Person</summary>
    [grpc::BindServiceMethod(typeof(Person), "BindService")]
    public abstract partial class PersonBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Tema1Server.PersonDataReply> Person(global::Tema1Server.PersonDataRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(PersonBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_Person, serviceImpl.Person).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, PersonBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_Person, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Tema1Server.PersonDataRequest, global::Tema1Server.PersonDataReply>(serviceImpl.Person));
    }

  }
}
#endregion
