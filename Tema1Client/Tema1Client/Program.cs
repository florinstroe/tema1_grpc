﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;

namespace Tema1Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            while (true)
            {
                string fullName;
                string CNP = "";

                Console.Write("Full name: ");
                fullName = Console.ReadLine();
                while (CNP.Length != 13)
                {
                    Console.Write("CNP: ");
                    CNP = Console.ReadLine();
                }

                using var channel = GrpcChannel.ForAddress("https://localhost:5001");
                var client = new Person.PersonClient(channel);
                var reply = await client.PersonAsync(
                                  new PersonDataRequest { FullName = fullName, Cnp = CNP });

                Console.WriteLine("Response from server: ");
                Console.WriteLine("Age: " + reply.Age);
                Console.WriteLine("Gender: " + reply.Gender);
                Console.WriteLine("----------------------------");
            }
        }
    }
}
